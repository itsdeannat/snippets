#!/bin/bash

CURRENT_ENV=$(pwd)

echo "Welcome templateer! We're excited to have you join the Community Docs working group"
echo "This is a script that can generate a directory to hold your template and template guide. Please follow the prompts as they appear in the terminal"
echo
sleep 0.5

echo "First, let's make sure you're currently in the templates repository"
echo "Checking location..."
sleep 0.5

if [[ "$CURRENT_ENV" == *templates ]] # If the current working directory ends with "templates"
then
	echo "You're in the right place"
else
    echo "I don't see a templates directory. I'll create one for you"
    mkdir templates
    cd templates 
    echo "Let's see if that worked"
    pwd
fi

echo "Let's create a directory for your template. What is the content type?"
read template_type # Creates a variable to store user input
mkdir $template_type; $CMD touch "${template_type}/${template_type}-"{template-guide,template}.md # Creates directory and files
echo "Let's make sure the files were created."
find ./"${template_type}"/ -type f -name "${template_type}*"

echo "success!"